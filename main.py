import asyncio
import websocket
import json
import csv
import ast
from binascii import crc32
import datetime
import copy
import os

def saveBook(bidsBook, asksBook):
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if not os.path.exists('record'):
        os.makedirs('record')
    with open("record/" + now + ".log", 'w') as file:
        file.write(json.dumps({'bids':bidsBook, 'asks':asksBook}))
        file.close()

# Call when the connection is established, initiallize the BOOK instance
# Send the Configuration and subscribe the channel
def on_open(ws):
    print("WebSockets established!")
    config = {
        'event': 'conf',
        'flags': 131072 # enable checksum
    }
    sub_book = {
        'event': 'subscribe', 
        'channel': 'book', 
        'symbol': 'tBTCUSD',
        'prec': 'P0'
    }
    BOOK['bids'] = {}
    BOOK['asks'] = {}
    BOOK['psnap'] = {}
    BOOK['mcnt'] = 0
    print("configuration with flag "+str(config['flags']))
    ws.send(json.dumps(config))
    print("Subscribe "+sub_book['channel'] + " channel")
    ws.send(json.dumps(sub_book))



def on_message(ws, evt):
    if evt[0] == '{': # Not book information
        return
    elif evt[0] == '[':
        msg = ast.literal_eval(evt) # resolve the response message 
        if msg[1] == 'hb':
            return
        if msg[1] == 'cs': # validation the checksum
            checksum = msg[2]
            csdata = []
            bidsKeys = BOOK['psnap']['bids']
            asksKeys = BOOK['psnap']['asks']
            for i in range(25):
                if bidsKeys[i]:
                    price = bidsKeys[i]
                    pp = BOOK['bids'][price]
                    csdata.append(str(pp['price']))
                    csdata.append(str(pp['amount']))
                if asksKeys[i]:
                    price = asksKeys[i]
                    pp = BOOK['asks'][price]
                    csdata.append(str(pp['price']))
                    csdata.append(str(-pp['amount']))

            csStr = ':'.join(csdata)
            csCalc = crc32(csStr.encode('utf8'))
            unsignCheckSum = checksum % (1<<32) # Convert the value to unsign integer
            if csCalc != unsignCheckSum:
                print('CHCEKSUM_FAILED')
            else:
                print('CHECKSUM_CORRECT')
                bidsBook = copy.deepcopy(BOOK['bids'])
                asksBook = copy.deepcopy(BOOK['asks'])
                saveBook(bidsBook, asksBook)
            return
        if BOOK['mcnt'] == 0: # Initialize the BOOK
            for pp in msg[1]:
                pp = {'price': pp[0], 'cnt': pp[1], 'amount': pp[2]}
                if pp['amount'] >= 0:
                    side = 'bids'
                else:
                    side = 'asks'
                pp['amount'] = abs(pp['amount'])
                BOOK[side][pp['price']] = pp
        else: # Update the BOOK for each response message
            msg = msg[1]
            pp = {'price': msg[0], 'cnt': msg[1], 'amount': msg[2]}
            if not pp['cnt']: # delete the book
                found = True

                if pp['amount'] > 0:
                    if BOOK['bids'][pp['price']]:
                        del BOOK['bids'][pp['price']]
                    else:
                        found = False
                elif pp['amount'] < 0:
                    if BOOK['asks'][pp['price']]:
                        del BOOK['asks'][pp['price']]
                    else:
                        found = False
                if not found:
                    print('Book delete failed. Price point not found')
            else: # update the book
                if pp['amount'] >= 0:
                    side = 'bids'
                else:
                    side = 'asks'
                pp['amount'] = abs(pp['amount'])
                BOOK[side][pp['price']] = pp

            for side in ['bids', 'asks']:
                sbook = BOOK[side]
                bprices = [*sbook]
                if side == 'bids':
                    prices = sorted(bprices, reverse=True)
                else:
                    prices = sorted(bprices)
                BOOK['psnap'][side] = prices
        BOOK['mcnt'] += 1

BOOK = {} # a order book instance

def main():
    ws = websocket.WebSocketApp('wss://api-pub.bitfinex.com/ws/2', on_open=on_open, on_message=on_message)
    ws.run_forever()




if __name__ == '__main__':
    main()
    




